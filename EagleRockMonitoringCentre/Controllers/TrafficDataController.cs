using EagleRock.Core;
using EagleRock.Core.Cache;
using Microsoft.AspNetCore.Mvc;

namespace EagleRockMonitoringCentre.Controllers
{
    /// <summary>
    /// Endpoint to query current traffic info and bots' locations
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class TrafficDataController : ControllerBase
    {

        private readonly ILogger<TrafficDataController> logger;
        private readonly ICacheStore cacheStore;

        public TrafficDataController(ILogger<TrafficDataController> logger, ICacheStore cacheStore)
        {
            this.logger = logger;
            this.cacheStore = cacheStore;
        }

        /// <summary>
        /// Gets current traffic reports from all of the EagleBots
        /// </summary>
        /// <returns>200 with <see cref="EagleBotReport"/>s or 500 with the message if an error was encountered</returns>
        [HttpGet]
        [Route("current")]
        public async Task<IActionResult> GetCurrentTrafficData()
        {
            try
            {
                // Returns latest data obtained from all of the bots connected to the hub
                var records = await cacheStore.GetGroup<EagleBotReport>(CacheNamingConventions.CurrentBotStateCacheGroupName);
                return Ok(records);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error getting current traffic data");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Returns current locations of all bots connected to the hub
        /// </summary>
        /// <returns>200 with encoded <see cref="EagleBotLocation"/>s or 500 with the message if an error was encountered</returns>
        [HttpGet]
        [Route("bots/locations")]
        public async Task<IActionResult> GetLocations()
        {
            try
            {
                var fullReports = await cacheStore.GetGroup<EagleBotReport>(CacheNamingConventions.CurrentBotStateCacheGroupName);
                var locations = fullReports.Select(x => EagleBotLocation.FromReport(x)).ToArray();
                return Ok(locations);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error getting current traffic data");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}