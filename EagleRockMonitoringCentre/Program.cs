using EagleRock.Core;
using EagleRock.Core.Cache;
using EagleRock.Core.Serialization;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

// Add controlers to the container.

builder.Services.AddControllers();

// Add services

builder.Services.AddSingleton<IPayloadSerializer, ProtobufSerializer>();
builder.Services.AddSingleton<ICacheStore>(x => ConfigureCacheStore(builder.Configuration, x.GetService<IPayloadSerializer>()));

var app = builder.Build();

app.MapControllers();

app.Run();

static ICacheStore ConfigureCacheStore(IConfiguration configuration, IPayloadSerializer serializer)
{
    var redisConnectionString = configuration.GetConnectionString(ConnectionKeys.RedisConnectionStringKey);
    ConfigurationOptions redisConfig = ConfigurationOptions.Parse(redisConnectionString);
    redisConfig.ClientName = "EagleRockMonitoringCentre";
    ConnectionMultiplexer redisHostConnection = ConnectionMultiplexer.Connect(redisConfig);
    return new AzureRedisCacheStore(redisHostConnection, serializer, TimeSpan.FromMinutes(10));
}
