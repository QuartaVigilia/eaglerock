using EagleRock.Core;
using EagleRock.Core.Cache;
using EagleRock.Core.Serialization;
using EagleRock.Core.Subscriptions;
using Microsoft.AspNetCore.Mvc;

namespace EagleRockHub.Controllers
{
    /// <summary>
    /// Endpoint to receive reports from the EagleBots
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ReportController : ControllerBase
    {
        private readonly ILogger<ReportController> logger;
        private readonly IPayloadSerializer serializer;
        private readonly ICacheStore cacheStore;
        private readonly IPublishService publishService;

        public ReportController(ILogger<ReportController> logger, IPayloadSerializer serializer, ICacheStore cacheStore, IPublishService publishService)
        {
            this.logger = logger;
            this.serializer = serializer;
            this.cacheStore = cacheStore;
            this.publishService = publishService;
        }

        /// <summary>
        /// Accepts the report from the EagleBot and stores it
        /// </summary>
        /// <param name="reportPayload">Serialized EagleBot report</param>
        /// <returns>201 Accepted on successful processing, 500 with an error message otherwise</returns>
        [HttpPost]
        [Route("submit")]
        public async Task<IActionResult> SubmitReport([FromBody] byte[] reportPayload)
        {
            try
            {
                // Verify that the received data is a valid report
                var report = serializer.Deserialize<EagleBotReport>(reportPayload);
                var historyKey = $"{report.BotId}-{report.Timestamp:O}";
                // Store the received data
                if (!await cacheStore.SetAsync(historyKey, report))
                    return StatusCode(StatusCodes.Status500InternalServerError);
                // Update the current status of the bot
                var currentStatusKey = report.BotId.ToString();
                if (!await cacheStore.AddToGroup(currentStatusKey, report, CacheNamingConventions.CurrentBotStateCacheGroupName))
                    return StatusCode(StatusCodes.Status500InternalServerError);
                // Let other clients know there is new data to process
                await publishService.PublishAsync(reportPayload);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error occured while submitting an EagleBot report");
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
            return StatusCode(StatusCodes.Status202Accepted);
        }
    }
}