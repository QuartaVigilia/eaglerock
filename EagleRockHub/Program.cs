using EagleRock.Core;
using EagleRock.Core.Cache;
using EagleRock.Core.Serialization;
using EagleRock.Core.Subscriptions;
using EagleRockHub;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

// Add controllers to the container.

builder.Services.AddControllers(options => options.InputFormatters.Insert(index: 0, new RawRequestBodyFormatter()));

// Configure dependencies
builder.Services.AddSingleton<IPayloadSerializer, ProtobufSerializer>();
builder.Services.AddSingleton<ICacheStore>(x => ConfigureCacheStore(builder.Configuration, x.GetService<IPayloadSerializer>()));
builder.Services.AddSingleton<IPublishService>(x => ConfigurePublishService(builder.Configuration, x.GetService<IPayloadSerializer>()));

var app = builder.Build();

// Configure the HTTP request pipeline.

app.MapControllers();

app.Run();

static ICacheStore ConfigureCacheStore(IConfiguration configuration, IPayloadSerializer serializer)
{
    var redisConnectionString = configuration.GetConnectionString(ConnectionKeys.RedisConnectionStringKey);
    ConfigurationOptions redisConfig = ConfigurationOptions.Parse(redisConnectionString);
    redisConfig.ClientName = "EagleRockHub";
    ConnectionMultiplexer redisHostConnection = ConnectionMultiplexer.Connect(redisConfig);
    return new AzureRedisCacheStore(redisHostConnection, serializer, TimeSpan.FromMinutes(10));
}

static IPublishService ConfigurePublishService(IConfiguration configuration, IPayloadSerializer serializer)
{
    var serviceBusConnectionString = configuration.GetConnectionString(ConnectionKeys.ServiceBusConnectionStringKey);
    var serviceBusTopic = configuration.GetConnectionString(ConnectionKeys.ServiceBusTopicKey);
    return new AzureServiceBusPublishService(serviceBusConnectionString, serviceBusTopic, serializer);
}

