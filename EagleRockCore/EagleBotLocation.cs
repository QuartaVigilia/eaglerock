﻿using ProtoBuf;

namespace EagleRock.Core
{
    /// <summary>
    /// Current location of the bot
    /// </summary>
    [ProtoContract(SkipConstructor = true)]
    public record EagleBotLocation
    {
        public EagleBotLocation(Guid botId, double longitude, double latitude, string roadName)
        {
            BotId = botId;
            Longitude = longitude;
            Latitude = latitude;
            RoadName = roadName;
        }

        /// <summary>
        /// Bot's unique identifier
        /// </summary>
        [ProtoMember(1)]
        public Guid BotId { get; init; }

        /// <summary>
        /// Longitude component of bot's current location
        /// </summary>
        [ProtoMember(2)]
        public double Longitude { get; init; }

        /// <summary>
        /// Latitude component of bot's current location
        /// </summary>
        [ProtoMember(3)]
        public double Latitude { get; init; }

        /// <summary>
        /// Name of the road currently inspected by the bot
        /// </summary>
        [ProtoMember(4)]
        public string RoadName { get; init; }

        /// <summary>
        /// Pulls required location info from the bot's report
        /// </summary>
        /// <param name="report">Full report to break down</param>
        /// <returns>Bot's current location</returns>
        public static EagleBotLocation FromReport(EagleBotReport report) => new(report.BotId,
                                                                                report.Longitude,
                                                                                report.Latitude,
                                                                                report.InspectedRoadName);
    }
}
