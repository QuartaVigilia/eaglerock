﻿namespace EagleRock.Core
{
    /// <summary>
    /// Direction of the flow of traffic in relation to the chosen basepoint e.g. city centre
    /// </summary>
    public enum TrafficFlowDirection
    {
        /// <summary>
        /// Bot failed to determine the direction of traffic flow
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Traffic flows outwards from the basepoint
        /// </summary>
        Outbound = 1,
        /// <summary>
        /// Traffic flows towards the base point
        /// </summary>
        Inbound = 2
    }
}
