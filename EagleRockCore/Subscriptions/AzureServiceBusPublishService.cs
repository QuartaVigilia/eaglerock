﻿using Azure.Messaging.ServiceBus;
using EagleRock.Core.Serialization;

namespace EagleRock.Core.Subscriptions
{
    /// <summary>
    /// Implementation of the <see cref="IPublishService"/> designed to work with Azure Service Bus Topic as a publish/subscribe mechanism
    /// </summary>
    public class AzureServiceBusPublishService : IPublishService, IAsyncDisposable
    {
        private readonly IPayloadSerializer serializer;

        // the client that owns the connection and can be used to create senders and receivers
        private readonly ServiceBusClient client;

        // The sender used to publish messages to the topic
        private readonly ServiceBusSender sender;

        /// <summary>
        /// Creates an instance of the publish service with provided parameters
        /// </summary>
        /// <param name="connectionString">Connection string to the Service Bus namespace</param>
        /// <param name="topicName">Name of the Service Bus topic</param>
        /// <param name="serializer">Serializer to use when encoding the payload</param>
        public AzureServiceBusPublishService(string connectionString, string topicName, IPayloadSerializer serializer)
        {
            this.serializer = serializer;
            client = new ServiceBusClient(connectionString);
            sender = client.CreateSender(topicName);
        }

        /// <summary>
        /// Sends a message with provided payload to the topic encoding it with the provided serializer
        /// Throws if type can not be encoded
        /// </summary>
        /// <typeparam name="T">Payload type to be encoded as a message content</typeparam>
        /// <param name="payload">Message payload to send</param>
        /// <returns>True if message was sent successfully, false otherwise</returns>
        public async Task<bool> PublishAsync<T>(T payload)
        {
            var payloadSerialized = serializer.Serialize(payload);
            return await PublishAsync(payloadSerialized);
        }

        /// <summary>
        /// Sends a message to the topic
        /// </summary>
        /// <param name="payload">Payload to include into the message</param>
        /// <returns>True if the message was sent successfully, False otherwise</returns>
        public async Task<bool> PublishAsync(byte[] payload)
        {
            var message = new ServiceBusMessage(payload);
            try
            {
                await sender.SendMessageAsync(message);
            }
            catch (ServiceBusException)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Disposes of the unmanaged resources used by the ServiceBus
        /// </summary>
        /// <returns></returns>
        public async ValueTask DisposeAsync()
        {
            await sender.DisposeAsync();
            await client.DisposeAsync();
        }
    }
}
