﻿namespace EagleRock.Core.Subscriptions
{
    /// <summary>
    /// Expected methods to work with a publish/subscribe model of messaging
    /// </summary>
    public interface IPublishService
    {
        /// <summary>
        /// Sends a message through the publishing pipeline
        /// </summary>
        /// <param name="payload">Message payload to send</param>
        /// <returns></returns>
        Task<bool> PublishAsync(byte[] payload);

        /// <summary>
        /// Serialises the type into the type supported by the publish service and sends a message
        /// </summary>
        /// <typeparam name="T">Payload type</typeparam>
        /// <param name="payload">Payload to send</param>
        /// <returns></returns>
        Task<bool> PublishAsync<T>(T payload);
    }
}
