﻿namespace EagleRock.Core.Cache
{
    /// <summary>
    /// Naming conventions to get rid of constants when working with cache
    /// </summary>
    public static class CacheNamingConventions
    {
        /// <summary>
        /// Name of the group of cached values current bot reports are added to
        /// </summary>
        public const string CurrentBotStateCacheGroupName = "CurrentState";
    }
}
