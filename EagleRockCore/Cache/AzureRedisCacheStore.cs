﻿using EagleRock.Core.Serialization;
using StackExchange.Redis;

namespace EagleRock.Core.Cache
{
    /// <summary>
    /// <see cref="ICacheStore"/> implementation to work with the Redis cache for Azure
    /// </summary>
    public class AzureRedisCacheStore : ICacheStore
    {
        private readonly ConnectionMultiplexer multiplexer;
        private readonly TimeSpan? expiryTime;
        private readonly IPayloadSerializer serializer;

        /// <summary>
        /// Creates a cache store to work with Azure Cache for Redis
        /// </summary>
        /// <param name="multiplexer">Connection manager</param>
        /// <param name="expiryTime">Time to live for an object after it was set or accessed last. Null means keep forever</param>
        /// <param name="serializer">Payload serializer to use when inserting a new or retrieving existing value</param>
        public AzureRedisCacheStore(ConnectionMultiplexer multiplexer, IPayloadSerializer serializer, TimeSpan? expiryTime = null)
        {
            this.multiplexer = multiplexer;
            this.serializer = serializer;
            this.expiryTime = expiryTime;
        }

        /// <summary>
        /// Adds provided key value pair to the Redis Hash
        /// </summary>
        /// <param name="key">Unique of an element to add</param>
        /// <param name="value">Value of an element to add</param>
        /// <param name="groupName"></param>
        /// <returns>True if the element was inserted successfully, False otherwise</returns>
        public async Task<bool> AddToGroup<T>(string key, T value, string groupName)
        {
            IDatabase cache = multiplexer.GetDatabase();
            var bytes = serializer.Serialize(value);
            try
            {
                // Both the insertion of the new key value pair
                // and the update of the existing value are favourable outcomes
                // only report failure if an internal or connection error happened
                await cache.HashSetAsync(groupName, key, bytes);
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets all values from the group of records stored as a RedisHash
        /// </summary>
        /// <typeparam name="T">Type to deserialise obtained records into</typeparam>
        /// <param name="groupName">Expected key of a hash</param>
        /// <returns>Collection of records stored in a hash if it exists and has content, empty collection otherwise</returns>
        public async Task<IEnumerable<T>> GetGroup<T>(string groupName)
        {
            var cache = multiplexer.GetDatabase();
            var group = await cache.HashValuesAsync(groupName);
            return group.Any() ? group.Select(x => serializer.Deserialize<T>(x)).ToArray() : Enumerable.Empty<T>();
        }

        /// <summary>
        /// Gets a record stored under the provided key
        /// </summary>
        /// <typeparam name="T">Type to deserialize the obtained record into</typeparam>
        /// <param name="key">Unique key of the record</param>
        /// <returns>Value of the record if it was found, default value for the class otherwise</returns>
        public async Task<T> GetAsync<T>(string key)
        {
            IDatabase cache = multiplexer.GetDatabase();
            //If we access it - it is useful, hence let it live 
            await cache.KeyExpireAsync(key, expiryTime, CommandFlags.FireAndForget);
            var response = await cache.StringGetAsync(key);
            if (!response.HasValue || response.IsNullOrEmpty)
            {
                return default;
            }
            return serializer.Deserialize<T>(response);
        }

        /// <summary>
        /// Sets the value for provided key to the provided value, overwrites the value if the key already exists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">Unique key of the record</param>
        /// <param name="value">Value to insert</param>
        /// <returns>True if the value was successfully set, False otherwise</returns>
        public async Task<bool> SetAsync<T>(string key, T value)
        {
            IDatabase cache = multiplexer.GetDatabase();
            var bytes = serializer.Serialize(value);
            return await cache.StringSetAsync(key, bytes, expiryTime);
        }

        /// <inheritdoc/>
        public async Task<bool> RemoveAsync(string key)
        {
            IDatabase cache = multiplexer.GetDatabase();
            return await cache.KeyDeleteAsync(key);
        }

        /// <inheritdoc/>
        public async Task<bool> Exists(string key)
        {
            IDatabase cache = multiplexer.GetDatabase();
            return await cache.KeyExistsAsync(key);
        }

        /// <inheritdoc/>
        public async Task<bool> RemoveGroup(string groupName)
        {
            // In the case of Redis Hash and a normal key value pair are equivalent when it comes to deletion
            return await RemoveAsync(groupName);
        }
    }
}
