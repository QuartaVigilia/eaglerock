﻿namespace EagleRock.Core.Cache
{
    /// <summary>
    /// Basic methods expected from the cache store
    /// </summary>
    public interface ICacheStore
    {
        /// <summary>
        /// Gets a value specified by the key from the cache
        /// </summary>
        /// <typeparam name="T">Type to cast the obtained value to</typeparam>
        /// <param name="key">Unique key of the cache entry</param>
        /// <returns>Value under the provided key or default value for the type if nothing was found</returns>
        Task<T> GetAsync<T>(string key);

        /// <summary>
        /// Sets a value to the specified key, creates a key value pair if it does not exist
        /// </summary>
        /// <typeparam name="T">Type of the value to insert</typeparam>
        /// <param name="key">Unique key</param>
        /// <param name="value">Value to insert</param>
        /// <returns>True if the value was set, False otherwise</returns>
        Task<bool> SetAsync<T>(string key, T value);

        /// <summary>
        /// Inserts a key value pair into the group with provided name. Creates the group if it does not exist.
        /// Ovewrites the value if the key is already present.
        /// </summary>
        /// <typeparam name="T">Type of the value to insert</typeparam>
        /// <param name="key">Unique key</param>
        /// <param name="value">Value to insert</param>
        /// <param name="groupName">Name of the group to insert the value into</param>
        /// <returns>True if the value was inserted, False otherwise</returns>
        Task<bool> AddToGroup<T>(string key, T value, string groupName);

        /// <summary>
        /// Gets all values from the group
        /// </summary>
        /// <typeparam name="T">Expected type of the value</typeparam>
        /// <param name="groupName">Name of the group</param>
        /// <returns>Collection of values from the group or empty collection if the group does not exist</returns>
        Task<IEnumerable<T>> GetGroup<T>(string groupName);

        /// <summary>
        /// Removes a key value pair with the provided key
        /// </summary>
        /// <param name="key">Key to remove</param>
        /// <returns>True if the value was removed successfully, False otherwise</returns>
        Task<bool> RemoveAsync(string key);

        /// <summary>
        /// Checks whether entry with the provided key exists
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>True if the key value pair exists, False otherwise</returns>
        Task<bool> Exists(string key);

        /// <summary>
        /// Removes a group with the provided name
        /// </summary>
        /// <param name="groupName">Name of the group to remove</param>
        /// <returns>True if the group was successfully removed, False otherwise</returns>
        Task<bool> RemoveGroup(string groupName);
    }
}
