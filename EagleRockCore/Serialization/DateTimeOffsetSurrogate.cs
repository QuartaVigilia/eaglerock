﻿using ProtoBuf;

namespace EagleRock.Core.Serialization
{
    /// <summary>
    /// Surrogate type to add support for the <see cref="DateTimeOffset"/> to the Protobuf.Net
    /// </summary>
    [ProtoContract]
    public class DateTimeOffsetSurrogate
    {
        [ProtoMember(1)]
        public string DateTimeString { get; set; }

        /// <summary>
        /// Converts provided <see cref="DateTimeOffset"/> into the ISO8601 string
        /// </summary>
        /// <param name="value">DateTime info to convert</param>
        public static implicit operator DateTimeOffsetSurrogate(DateTimeOffset value)
        {
            return new DateTimeOffsetSurrogate { DateTimeString = value.ToString("o") };
        }

        /// <summary>
        /// Converts the surrogate ISO8601 string into the <see cref="DateTimeOffset"/>
        /// with corresponding data
        /// </summary>
        /// <param name="value">Surrogate to revive</param>
        public static implicit operator DateTimeOffset(DateTimeOffsetSurrogate value)
        {
            return DateTimeOffset.Parse(value.DateTimeString);
        }
    }
}
