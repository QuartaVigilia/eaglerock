﻿namespace EagleRock.Core.Serialization
{
    /// <summary>
    /// Set of methods expected from a payload serializer
    /// </summary>
    public interface IPayloadSerializer
    {
        /// <summary>
        /// Serializes provided item into the byte array
        /// </summary>
        /// <typeparam name="T">Type to serialize from</typeparam>
        /// <param name="item">Item to serialize</param>
        /// <returns>A binary representation of a provided item</returns>
        byte[] Serialize<T>(T item);

        /// <summary>
        /// Deserializes provided byte array into the requested type
        /// </summary>
        /// <typeparam name="T">Type to deserialize into</typeparam>
        /// <param name="buffer">Serialized record</param>
        /// <returns>An instance of the requested type deserialized from the provided binary data</returns>
        T Deserialize<T>(byte[] buffer);
    }
}
