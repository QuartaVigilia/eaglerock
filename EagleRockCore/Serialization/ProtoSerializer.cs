﻿using ProtoBuf;
using ProtoBuf.Meta;

namespace EagleRock.Core.Serialization
{
    /// <summary>
    /// Serializer using Protocol Buffers as a serialization method
    /// </summary>
    public class ProtobufSerializer : IPayloadSerializer
    {
        /// <summary>
        /// Registers surrogates required for the serialization to work
        /// </summary>
        static ProtobufSerializer()
        {
            // Protobuf can't always handle standard dotnet types, so we have to educate it
            // for that purpose we have a surrogate class which is serialized instead of
            // the target class, but then deserealized to the instance of a target class
            // e.g. DateTimeOffset is serialized as a string and then deserialized as a DateTimeOffset
            RuntimeTypeModel.Default.Add(typeof(DateTimeOffset), false).SetSurrogate(typeof(DateTimeOffsetSurrogate));
            Serializer.PrepareSerializer<DateTimeOffset>();
        }

        /// <inheritdoc/>
        public byte[] Serialize<T>(T item)
        {
            using var stream = new MemoryStream();
            Serializer.Serialize(stream, item);
            var buff = stream.ToArray();
            return buff;
        }

        /// <inheritdoc/>
        public T Deserialize<T>(byte[] buffer)
        {
            using var stream = new MemoryStream(buffer);
            stream.Position = 0;
            return Serializer.Deserialize<T>(stream);
        }
    }
}
