﻿namespace EagleRock.Core
{
    /// <summary>
    /// Collection of keys for easier configuration mapping
    /// </summary>
    public static class ConnectionKeys
    {
        /// <summary>
        /// Connection string to use with Azure Redis Cache
        /// </summary>
        public const string RedisConnectionStringKey = "RedisConnectionString";

        /// <summary>
        /// Connection string to use with Azure Service Bus namespaces
        /// </summary>
        public const string ServiceBusConnectionStringKey = "ServiceBusConnectionString";

        /// <summary>
        /// Name of the topic to use with a Service Bus Client
        /// </summary>
        public const string ServiceBusTopicKey = "ServiceBusTopic";
    }
}
