﻿using ProtoBuf;

namespace EagleRock.Core
{
    /// <summary>
    /// Snapshot of current road conditions transferred from the EagleBot
    /// </summary>
    [ProtoContract(SkipConstructor = true)]
    public record EagleBotReport
    {
        public EagleBotReport(Guid botId,
                              double longitude,
                              double latitude,
                              DateTimeOffset timestamp,
                              string inspectedRoadName,
                              TrafficFlowDirection flowDirection,
                              int rateOfTrafficFlow,
                              int averageVehicleSpeed)
        {
            BotId = botId;
            Longitude = longitude;
            Latitude = latitude;
            Timestamp = timestamp;
            InspectedRoadName = inspectedRoadName;
            FlowDirection = flowDirection;
            RateOfTrafficFlow = rateOfTrafficFlow;
            AverageVehicleSpeed = averageVehicleSpeed;
        }

        /// <summary>
        /// Bot's unique identifier
        /// </summary>
        [ProtoMember(1)]
        public Guid BotId { get; init; }

        /// <summary>
        /// Longitude part of the bot's current location
        /// </summary>
        [ProtoMember(2)]
        public double Longitude { get; init; }

        /// <summary>
        /// Latitude part of bot's current location
        /// </summary>
        [ProtoMember(3)]
        public double Latitude { get; init; }

        /// <summary>
        /// Timestamp indicating the moment of the data exchange
        /// </summary>
        [ProtoMember(4)]
        public DateTimeOffset Timestamp { get; init; }

        /// <summary>
        /// Name of the road being inspected
        /// </summary>
        [ProtoMember(5)]
        public string InspectedRoadName { get; init; }

        /// <summary>
        /// The direction monitored traffic is flowing to
        /// </summary>
        [ProtoMember(6)]
        public TrafficFlowDirection FlowDirection { get; init; }

        /// <summary>
        /// Amount of vehicles passing the point of observation per hour
        /// </summary>
        [ProtoMember(7)]
        public int RateOfTrafficFlow { get; init; }

        /// <summary>
        /// Average speed of a passing vehicle expressed in kph
        /// </summary>
        [ProtoMember(8)]
        public int AverageVehicleSpeed { get; init; }
    }
}