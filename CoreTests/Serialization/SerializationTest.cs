using EagleRock.Core;
using EagleRock.Core.Serialization;
using System;
using Xunit;

namespace CoreTests
{
    /// <summary>
    /// Tests that the core domain classes are serializable
    /// </summary>
    public class SerializationTest
    {
        private readonly IPayloadSerializer serializer;

        public SerializationTest()
        {
            serializer = new ProtobufSerializer();
        }

        private static EagleBotReport MakeSampleReport() => new(botId: Guid.NewGuid(),
                                                                longitude: 1,
                                                                latitude: 3,
                                                                timestamp: DateTimeOffset.Now,
                                                                inspectedRoadName: "Some road",
                                                                flowDirection: TrafficFlowDirection.Outbound,
                                                                rateOfTrafficFlow: 1,
                                                                averageVehicleSpeed: 100);

        public static EagleBotLocation MakeSampleLocation() => new(botId: Guid.NewGuid(),
                                                                   longitude: 1,
                                                                   latitude: 2,
                                                                   roadName: "Road");

        [Fact]
        public void EagleBotReportCanBeSerialized()
        {
            var report = MakeSampleReport();
            var bytes = serializer.Serialize(report);
            Assert.NotNull(bytes);
            Assert.NotEmpty(bytes);
        }

        [Fact]
        public void EagleBotReportCanBeDeserialized()
        {
            var report = MakeSampleReport();
            var bytes = serializer.Serialize(report);
            var reportBack = serializer.Deserialize<EagleBotReport>(bytes);
            Assert.Equal(report, reportBack);
        }

        [Fact]
        public void EagleBotLocationCanBeSerialized()
        {
            var location = MakeSampleLocation();
            var bytes = serializer.Serialize(location);
            Assert.NotNull(bytes);
            Assert.NotEmpty(bytes);
        }

        [Fact]
        public void EagleBotLocationCanBeDeserialized()
        {
            var location = MakeSampleLocation();
            var bytes = serializer.Serialize(location);
            var locationBack = serializer.Deserialize<EagleBotLocation>(bytes);
            Assert.Equal(location, locationBack);
        }
    }
}